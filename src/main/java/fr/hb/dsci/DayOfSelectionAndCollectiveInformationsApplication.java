package fr.hb.dsci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DayOfSelectionAndCollectiveInformationsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DayOfSelectionAndCollectiveInformationsApplication.class, args);
    }

}
